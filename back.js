const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const prometheus = require('prom-client');

// Initialise l'objet prometheus.register
prometheus.collectDefaultMetrics();

mongoose.connect('mongodb://root:rootpassword@mongo:27017', { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => console.log('Connexion à MongoDB réussie'))
  .catch(() => console.log('Connexion à MongoDB échouée'));

const petitionSchema = new mongoose.Schema({
  title: String,
  description: String,
  id: String,
  image: String,
  yes: Number,
  no: Number,
  EnCours: Boolean
});

const Petition = mongoose.model('Petition', petitionSchema);
const app = express();

app.use(bodyParser.json());

app.get('/healthcheck', async (req, res) => {
  try {
      const dbStatus = await mongoose.connection.readyState;
      if (dbStatus !== 1) {
        return res.status(500).json({ message: 'Base de données hors ligne' }).send();
      }

      res.json({ message: 'Base de données en ligne' }).send();
    } catch (err) {
      res.status(500).json({ message: err.message }).send();
    }
});

app.post('/createPetition', async (req, res) => {
  console.log("create ",req.body)
  const petition = new Petition({
    title: req.body.title,
    description: req.body.description,
    id: req.body.id,
    image: req.body.image,
    yes: 0,
    no: 0,
    EnCours: true
  });

  try {
    const result = await petition.save();
    res.status(201).send();
    apiPetitionInProgress.inc();
  } catch (err) {
    res.status(400).send();
  }
});

app.get('/getPetitions', async (req, res) => {
  try {
    const petitions = await Petition.find();
    res.json(petitions);
  } catch (err) {
    res.status(500).send();
  }
});

app.put('/updatePetition', async (req, res) => {
  try {
    console.log("update ", req.body)
    const petition = await Petition.find({id:req.body.id});
    if (!petition) return res.status(404).json({ message: 'non trouvé' }).send();

    await Petition.updateOne({id:req.body.id}, {
      yes :req.body.yes,
      no : req.body.no,
      EnCours : false,
    });
  } catch (err) {
    res.status(500).send();
  }
});

app.listen(8080, () => console.log('Serveur démarré sur le port 8080'));

// region metrics
app.get('/metrics', async (req, res) => {
  try {
    res.set('Content-Type', prometheus.register.contentType);
    const metrics = await prometheus.register.metrics();
    res.send(metrics);
  } catch (err) {
    console.error(err);
    res.status(500).end();
  }
});

const apiPetitionInProgress = new prometheus.Counter({
  name: 'api_petition_inprogress',
  help: 'Total number of petition in progress'
});

const apiPetitionUpdated = new prometheus.Counter({
  name: 'api_petition_updated',
  help: 'Total number of petition updated'
});

// endregion