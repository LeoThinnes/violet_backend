FROM node:latest

WORKDIR /usr/src/back

COPY package.json ./
COPY package-lock.json ./

RUN npm i

COPY back.js ./
COPY health.js ./

EXPOSE 8080

ENTRYPOINT [ "node", "back.js" ]

HEALTHCHECK --interval=30s --timeout=10s CMD node health.js || exit 1