var http = require("http");


var request = http.request('http://localhost:8080/healthcheck/', (res) => {
  console.log(`STATUS: ${res.statusCode}`);
  if (res.statusCode == 200) {
    process.exit(0);
  } else {
    process.exit(1);
  }
});
request.end();